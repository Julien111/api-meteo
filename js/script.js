import { apiKey } from "./variable.js";

const APIKEY = apiKey();

$(document).ready(function () {
  //Quand on clique on affiche la météo d'une ville choisie par l'utilisateur

  $("#myButton").on("click", function () {
    let city = document.getElementById("inputCity").value;

    let weatherIcons = {
      rain: "wi wi-day-rain",
      clouds: "wi wi-day-cloudy",
      clear: "wi wi-day-sunny",
      snow: "wi wi-day-snow",
      mist: "wi wi-day-fog",
      drizzle: "wi wi-day-sleet",
      thunder: "wi wi-day-thunderstorm",
      sun: "wi wi-hot",
    };

    
    if (city === "") {
      city = annonay;
    }

    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&lang=fr&units=metric&appid=${APIKEY}`;

    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        document.getElementById("city").innerHTML =
          "<i class='fas fa-home'></i>" + data.name;
        document.getElementById("temp").innerHTML =
          "<i class='fas fa-temperature-high'></i>" + data.main.temp + " C°";
        document.getElementById("humidity").innerHTML =
          "<i class='fas fa-tint'></i>" + data.main.humidity + " %";
        document.getElementById("wind").innerHTML =
          "<i class='fas fa-wind'></i>" + data.wind.speed + " Km/h";
        document.getElementById("weather").innerHTML =
          data.weather[0].description;

        //test affiche une image de fond différente selon le temps

        console.log(data.weather[0].description);

        if (
          data.weather[0].description == "nuageux" ||
          data.weather[0].description == "peu nuageux" ||
          data.weather[0].description == "couvert"
        ) {
          $("body").css("background", "url(images/cloud.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.clouds}'></i><br><p>Couvert</p>`;
        }
        if (
          data.weather[0].description == "brouillard" ||
          data.weather[0].description == "brume"
        ) {
          $("body").css("background", "url(images/brouillard.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.mist}'></i><br><p>Brouillard</p>`;
        }
        if (
          data.weather[0].description == "bruine légère" ||
          data.weather[0].description == "bruine"
        ) {
          $("body").css("background", "url(images/brouillard.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.drizzle}'></i><br><p>Bruine</p>`;
        }

        if (
          data.weather[0].description == "pluie" ||
          data.weather[0].description == "légère pluie" ||
          data.weather[0].description == "averses de pluie"
        ) {
          $("body").css("background", "url(images/pluie.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.rain}'></i><br><p>Pluie</p>`;
        }

        if (
          data.weather[0].description == "ciel dégagé" ||
          data.weather[0].description == "partiellement nuageux"
        ) {
          $("body").css("background", "url(images/soleil.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.clear}'></i><br><p>Ciel dégagé</p>`;
        }

        if (data.weather[0].description == "soleil") {
          $("body").css("background", "url(images/ciel.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.sun}'></i><br><p>Grand soleil</p>`;
        }

        if (
          data.weather[0].description == "orageux" ||
          data.weather[0].description == "orage"
        ) {
          $("body").css("background", "url(images/orage.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.thunder}'></i><br><p>Orage</p>`;
        }

        if (
          data.weather[0].description == "neige" ||
          data.weather[0].description == "averse neige" ||
          data.weather[0].description == "neige légère" ||
          data.weather[0].description == "légères chutes de neige"
        ) {
          $("body").css("background", "url(images/neige.jpg)");
          document.querySelector(
            "#weather"
          ).innerHTML = `<i class='${weatherIcons.snow}'></i><br><p>Neige</p>`;
        }

        document.getElementById("pression").innerHTML =
          data.main.pressure + " hPa";
      })
      .catch((err) => console.log("Erreur : " + err));
  });
});

//On affiche la météo d'annonay par défaut

let cityDefault = "annonay";

let apiKey2 = APIKEY;

let url = `http://api.openweathermap.org/data/2.5/weather?q=${cityDefault}&lang=fr&units=metric&appid=${apiKey2}`;

fetch(url)
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
    document.getElementById("city").innerHTML =
      "<i class='fas fa-home'></i>" + data.name;
    document.getElementById("temp").innerHTML =
      "<i class='fas fa-temperature-high'></i>" + data.main.temp + " C°";
    document.getElementById("humidity").innerHTML =
      "<i class='fas fa-tint'></i>" + data.main.humidity + " %";
    document.getElementById("wind").innerHTML =
      "<i class='fas fa-wind'></i>" + data.wind.speed + " Km/h";
    document.getElementById("weather").innerHTML = data.weather[0].description;
    document.getElementById("pression").innerHTML = data.main.pressure + " hPa";
  })
  .catch((err) => console.log("Erreur : " + err));
